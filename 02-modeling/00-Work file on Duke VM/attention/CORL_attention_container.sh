#!/bin/bash
#SBATCH -p scavenger
#SBATCH --mem=20G

singularity exec --bind /work:/work,/work/th264/repository/CORL/attention:/output /work/th264/repository/CORL/attention/CORL_attention.sif bash /work/th264/repository/CORL/attention/CORL_attention.sh

