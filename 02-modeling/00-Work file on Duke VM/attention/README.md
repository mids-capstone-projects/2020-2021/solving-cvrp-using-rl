# Attention, Learn to Solve Routing Problems!

Attention based model for learning to solve the Travelling Salesman Problem (TSP) and the Vehicle Routing Problem (VRP), Orienteering Problem (OP) and (Stochastic) Prize Collecting TSP (PCTSP). Training with REINFORCE with greedy rollout baseline.

This repository adjusted [the original model](https://github.com/wouterkool/attention-learn-to-route) to make it run on Duke Compute Cluster (DCC), which is a HPC with slurm system. 
To replicate the run on DCC or the same system, one should follow the steps:

- Upload the repository to the system.
- If needed, obtain the environment for the model training by Docker or Singularity.
- Set hyperparameters for the model by modifing CORL_attention.sh. 
- Run CORL_attention_container.sh.
