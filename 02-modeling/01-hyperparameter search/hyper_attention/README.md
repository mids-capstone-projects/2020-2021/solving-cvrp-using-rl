# Hyperparameter Search on attention to learn model.

This repository applied Weights & Bias to  [the original model](https://github.com/wouterkool/attention-learn-to-route) to visualize the training process and conduct hyperparameter search.
To conduct hyperparameter search using Google Colab, one should follow the steps:

- Upload the repository to Google drive.
- Set hyperparameters for the model using .yaml file. Please refer to the documentation on Weights & Bias website. "sweep-bayesian.yaml" and "sweep-grid-hyperband.yaml" are two example files.
- Open "hyper_attention.ipynb" in Google Colab and start hyperparameter search
