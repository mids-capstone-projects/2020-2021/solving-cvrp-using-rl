# Garage and Attention repository set up

To set up attention model on Google colab, use the Attention.ipynb notebook to clone the repositories for Attention and Garage. This sets up Attention as a submodule of the Garage module, which allows you to import functions or scripts from Garage.

The cells will walk you through the set up. 
