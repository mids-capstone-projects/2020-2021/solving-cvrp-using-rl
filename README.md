## Final Report
Find the final report for this project [here](https://gitlab.oit.duke.edu/mids-capstone-projects/2020-2021/solving-cvrp-using-rl/-/blob/master/03-report-and-deliverables/Using%20Reinforcement%20Learning%20to%20Solve%20Capacitated%20Vehicle%20Routing%20Problem.pdf).



## Exploratory data analysis (EDA)
Find the project EDA [here](https://gitlab.oit.duke.edu/mids-capstone-projects/2020-2021/solving-cvrp-using-rl/-/tree/master/01-eda).



## Hyperparameter Search 
Find the work on hyperparameter search [here](https://gitlab.oit.duke.edu/mids-capstone-projects/2020-2021/solving-cvrp-using-rl/-/tree/master/02-modeling/01-hyperparameter%20search/hyper_attention).



## TRPO
Find the work on applying TRPO. [here](https://gitlab.oit.duke.edu/mids-capstone-projects/2020-2021/solving-cvrp-using-rl/-/tree/master/02-modeling/02-trpo).

